/*----------------------------------------------------------------------------*/
/* Copyright (c) 2018-2019 FIRST. All Rights Reserved.                        */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot;

import edu.wpi.first.wpilibj.GenericHID;
import edu.wpi.first.wpilibj.Joystick;
import edu.wpi.first.wpilibj.XboxController;
import edu.wpi.first.wpilibj2.command.Command;
import edu.wpi.first.wpilibj2.command.button.JoystickButton;
import frc.robot.commands.*;
import frc.robot.subsystems.*;

/**
 * This class is where the bulk of the robot should be declared.  Since Command-based is a
 * "declarative" paradigm, very little robot logic should actually be handled in the {@link Robot}
 * periodic methods (other than the scheduler calls).  Instead, the structure of the robot
 * (including subsystems, commands, and button mappings) should be declared here.
 */
public class RobotContainer 
{
  // private final FlapClass mainFlap = new FlapClass();

  // The robot's subsystems and commands are defined here...
  private final ExampleSubsystem m_exampleSubsystem = new ExampleSubsystem();
  private final DriveSubsystem mainDriveSubsystem = new DriveSubsystem();
  private final ControlPanelSubsystem mainControlPanelSubsystem = new ControlPanelSubsystem();
  //private final ShootingSubsystem mainShootingSubsystem = new ShootingSubsystem();
  //private final CollectorSubsystem mainCollectorSubsystem = new CollectorSubsystem();
  private final FiringSubsystem mainFiringSubsystem = new FiringSubsystem();

  private final AutonomousDriveCommand mainAutonomousDriveCommand= new AutonomousDriveCommand(mainDriveSubsystem, 3000);
  private final ExampleCommand m_autoCommand = new ExampleCommand(m_exampleSubsystem);
  private final TeleopDriveCommand mainTeleopDriveCommand = new TeleopDriveCommand(mainDriveSubsystem);
  private final RotateTimesCommand mainRotateTimesCommand = new RotateTimesCommand(mainControlPanelSubsystem);
  private final RotateColorCommand mainRotateColorCommand = new RotateColorCommand(mainControlPanelSubsystem);
  
  private final FireCommand mainFiringCommand = new FireCommand(mainFiringSubsystem);
  private final StopCollectionCommand mainStopCollectionCommand = new StopCollectionCommand(mainFiringSubsystem);
  private final StartCollectionCommand mainStartCollectionCommand = new StartCollectionCommand(mainFiringSubsystem);
  private final SpitCollectedCommand mainSpitCommand = new SpitCollectedCommand(mainFiringSubsystem);
 
  private final DriveAndShootMidleCommand mainDriveAndShoot = new DriveAndShootMidleCommand(new FireCommand(mainFiringSubsystem), mainAutonomousDriveCommand);

  public static Joystick driverJoystick = new Joystick(Constants.driverJoystickPort);
  public static Joystick commanderJoystick = new Joystick(Constants.commanderJoystickPort);

  /**
   * The container for the robot.  Contains subsystems, OI devices, and commands.
   */
  public RobotContainer() {
    // Configure the button bindings
    configureButtonBindings();
    
  }

  /**
   * Use this method to define your button->command mappings.  Buttons can be created by
   * instantiating a {@link GenericHID} or one of its subclasses ({@link
   * edu.wpi.first.wpilibj.Joystick} or {@link XboxController}), and then passing it to a
   * {@link edu.wpi.first.wpilibj2.command.button.JoystickButton}.
   */
  private void configureButtonBindings() {
    JoystickButton driverTrigger = new JoystickButton(driverJoystick, Constants.driverTriggerNum);
    driverTrigger.whileActiveContinuous(mainTeleopDriveCommand);
    
    JoystickButton rotateTimesButton = new JoystickButton(commanderJoystick, Constants.rotateTimesButtonNum);
    rotateTimesButton.whenPressed(mainRotateTimesCommand);

    JoystickButton rotateColorButton = new JoystickButton(commanderJoystick, Constants.rotateColorButtonNum);
    rotateColorButton.whenPressed(mainRotateColorCommand);
    
    JoystickButton startSpitButton = new JoystickButton(commanderJoystick, Constants.spitButton);
    startSpitButton.whenPressed(this.mainSpitCommand);

    JoystickButton startCollectButton = new JoystickButton(commanderJoystick, Constants.startCollectingButton);
    startCollectButton.whenPressed(this.mainStartCollectionCommand);

    JoystickButton stopCollectButton = new JoystickButton(commanderJoystick, Constants.stopCollectingButton);
    stopCollectButton.whenPressed(this.mainStopCollectionCommand);

    JoystickButton firingButton = new JoystickButton(commanderJoystick, Constants.shooterButtonNum);
    firingButton.whileActiveContinuous(mainFiringCommand);

    
  }


  /**
   * Use this to pass the autonomous command to the main {@link Robot} class.
   *
   * @return the command to run in autonomous
   */
  public Command getAutonomousCommand() {
    // An ExampleCommand will run in autonomous
    return mainDriveAndShoot ;
  }
}
