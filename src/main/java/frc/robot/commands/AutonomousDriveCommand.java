package frc.robot.commands;

import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.subsystems.DriveSubsystem;
import frc.robot.Constants;

public class AutonomousDriveCommand extends CommandBase {
    private final DriveSubsystem m_subsystem;
    private double m_timeToDrive;
    private long m_startTime;

    public AutonomousDriveCommand(DriveSubsystem subsystem, double timeToDrive){
        m_subsystem = subsystem;    
        addRequirements(subsystem);
        this.m_timeToDrive = timeToDrive;
        
    }
    public void initialize() {
        this.m_startTime = System.currentTimeMillis();
    }

    // Called repeatedly when this Command is scheduled to run
    public void execute() {
        
        m_subsystem.teleopDrive(Constants.wheelsSpeed, 0);

    }
    // Make this return true when this Command no longer needs to run execute()
    public boolean isFinished() {
        if(System.currentTimeMillis() - this.m_startTime > this.m_timeToDrive){

            //m_subsystem.teleopDrive(-1, 0);
            return true;
        }
        else{
            return false;
        }
    }

    // Called once after isFinished returns true
    public void end() {
        m_subsystem.teleopDrive(0, 0);
    }

    // Called when another command which requires one or more of the same
    // subsystems is scheduled to run
    public void interrupted() {
    }



}