/*----------------------------------------------------------------------------*/
/* Copyright (c) 2018-2019 FIRST. All Rights Reserved.                        */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.commands;

import frc.robot.subsystems.ExampleSubsystem;
import edu.wpi.first.wpilibj2.command.CommandBase;
import edu.wpi.first.wpilibj2.command.ParallelDeadlineGroup;
import edu.wpi.first.wpilibj2.command.SequentialCommandGroup;
import edu.wpi.first.wpilibj2.command.WaitCommand;
import frc.robot.subsystems.*;
import frc.robot.commands.*;

/**
 * An example command that uses an example subsystem.
 */
public class DriveAndShootMidleCommand extends SequentialCommandGroup {

  FireCommand m_fireCommand;
  AutonomousDriveCommand m_autonomousDriveCommand;

  public DriveAndShootMidleCommand(FireCommand fireCommand, AutonomousDriveCommand autonomousDriveCommand) {
    // Use addRequirements() here to declare subsystem dependencies.
    this.m_fireCommand = fireCommand;
    this.m_autonomousDriveCommand = autonomousDriveCommand;
    addCommands(this.m_autonomousDriveCommand, new ParallelDeadlineGroup( new WaitCommand(7), this.m_fireCommand));
  }

//   // Called when the command is initially scheduled.
//   @Override
//   public void initialize() {
//   }

//   // Called every time the scheduler runs while the command is scheduled.
//   @Override
//   public void execute() {
//   }

//   // Called once the command ends or is interrupted.
//   @Override
//   public void end(boolean interrupted) {
//   }

//   // Returns true when the command should end.
//   @Override
//   public boolean isFinished() {
//     return false;
//   }
}
