/*----------------------------------------------------------------------------*/
/* Copyright (c) 2018 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.commands;

import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.Constants;
import frc.robot.subsystems.FiringSubsystem;

public class StartCollectionCommand extends CommandBase {
  private FiringSubsystem mainSubsystem;

  public StartCollectionCommand(FiringSubsystem subsystem) 
  {
    this.mainSubsystem = subsystem;
  }

  // Called just before this Command runs the first time
  public void initialize() {
    //Close Flap
  }

  // Called repeatedly when this Command is scheduled to run
  public void execute() {
    //Strat The motars for colecting
    this.mainSubsystem.collectorOn(Constants.collectionTopSpeed, Constants.collectionBotSpeed);
    this.mainSubsystem.magazineOn(Constants.magazineSpeed);
  }

  // Make this return true when this Command no longer needs to run execute()
  public boolean isFinished() { 
    return true;    //Commands execute shuld run only ones
  }

  // Called once after isFinished returns true
  public void end() {
  }

}
