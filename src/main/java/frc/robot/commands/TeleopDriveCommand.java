package frc.robot.commands;

import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.RobotContainer;
import frc.robot.subsystems.DriveSubsystem;
import frc.robot.Constants;

public class TeleopDriveCommand extends CommandBase {
    private final DriveSubsystem m_subsystem;

    
    public TeleopDriveCommand(DriveSubsystem subsystem){
        m_subsystem = subsystem;    
        addRequirements(subsystem);

    }
    public void initialize() {
    }

    // Called repeatedly when this Command is scheduled to run
    public void execute() {
        
        if(RobotContainer.driverJoystick.getRawButton(Constants.flipButton)){
            m_subsystem.teleopDrive(RobotContainer.driverJoystick.getY(), RobotContainer.driverJoystick.getX()*0.7);
        }

        else{
            m_subsystem.teleopDrive(RobotContainer.driverJoystick.getY()*-1, RobotContainer.driverJoystick.getX()*0.7);
    
        }
    }
    // Make this return true when this Command no longer needs to run execute()
    public boolean isFinished() {
        return false;
    }

    // Called once after isFinished returns true
    protected void end() {
    }

    // Called when another command which requires one or more of the same
    // subsystems is scheduled to run
    protected void interrupted() {
    }



}