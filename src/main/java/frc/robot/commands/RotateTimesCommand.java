package frc.robot.commands;

import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.Constants;
import frc.robot.subsystems.ControlPanelSubsystem;

public class RotateTimesCommand extends CommandBase{
    private final ControlPanelSubsystem m_subsystem;
    private int startingColor;
    private boolean seeStartingColor;
    private int halfTurns;
    // private int i;

    public RotateTimesCommand(ControlPanelSubsystem subsystem){
        this.m_subsystem = subsystem;    
        addRequirements(subsystem);
        
    }

    public void initialize() {
        this.startingColor = this.m_subsystem.getColor();
        this.seeStartingColor = true;
        this.halfTurns = 0;
        // this.i = 0;
    }

    // Called repeatedly when this Command is scheduled to run
    public void execute() 
    {
        
        if ((this.m_subsystem.getColor() == startingColor)&&(!this.seeStartingColor))
        {
            this.seeStartingColor = true;
            this.halfTurns++;
        }
        if ((this.m_subsystem.getColor() != startingColor)&&(this.seeStartingColor))
        {
            this.seeStartingColor = false;
        }
        this.m_subsystem.cpMotorOn(Constants.cpMotorSpeed);
        SmartDashboard.putNumber("Half Turns", this.halfTurns);

        // this.m_subsystem.cpMotorOn(0.27);
        // if(this.i%30 == 0)
        // {
            
        //     this.m_subsystem.cpMotorOff();
        // }
    }

    // Make this return true when this Command no longer needs to run execute()
    public boolean isFinished() {
        
        if (this.halfTurns >= 7){
            //this.m_subsystem.cpMotorOff();
            return true;
        }
        return false;
        // if(this.i == 10)
        // {
        //     return true;
        // }
        // else
        // {
        //     this.i++;
        //     return false;
        // }

    }

    // Called once after isFinished returns true
    @Override
    public void end(boolean interrupted) {
        System.out.println("Done Rotating!");
        this.m_subsystem.cpMotorOff();
    }

    // Called when another command which requires one or more of the same
    // subsystems is scheduled to run
    public void interrupted() {
    }
}