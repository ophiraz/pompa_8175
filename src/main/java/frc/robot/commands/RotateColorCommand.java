package frc.robot.commands;

import edu.wpi.first.wpilibj.DriverStation;
import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.Constants;
import frc.robot.subsystems.ControlPanelSubsystem;

public class RotateColorCommand extends CommandBase{
    private final ControlPanelSubsystem m_subsystem;
    private String gameData;
    private int color;
    private boolean recievedColor;

    public RotateColorCommand(ControlPanelSubsystem subsystem){
        this.m_subsystem = subsystem;    
        addRequirements(subsystem);
        
    }

    public void initialize() {
        this.gameData = DriverStation.getInstance().getGameSpecificMessage();
        this.recievedColor = true;
        switch (gameData.charAt(0)){
            case 'B' :
            this.color = 2;
            break;
            case 'G' :
            this.color = 3;
            break;
            case 'R' :
            this.color = 0;
            break;
            case 'Y' :
            this.color = 1;
            break;
            default :
            this.recievedColor = false;
            break;
        }
    }

    // Called repeatedly when this Command is scheduled to run
    public void execute() {
        if (!(this.m_subsystem.getColor()==this.color)){
            this.m_subsystem.cpMotorOn(Constants.cpMotorSpeed);
        }
    }

    // Make this return true when this Command no longer needs to run execute()
    public boolean isFinished() {
        if (!this.recievedColor){
            // this.m_subsystem.cpMotorOff();
            this.m_subsystem.cpMotorOn(-1);
            return true;
        }
        if (this.m_subsystem.getColor()==this.color){
            // this.m_subsystem.cpMotorOff();
            this.m_subsystem.cpMotorOn(-1);
            return true;
        }
        return false;
    }

    // Called once after isFinished returns true
    @Override
    public void end(boolean interrupted) {
        this.m_subsystem.cpMotorOff();
    }

    // Called when another command which requires one or more of the same
    // subsystems is scheduled to run
    public void interrupted() {
    }
}