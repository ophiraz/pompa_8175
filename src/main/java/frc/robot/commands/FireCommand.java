package frc.robot.commands;

import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.Constants;
import frc.robot.subsystems.FiringSubsystem;

/**
 * An example command that uses an example subsystem.
 */
public class FireCommand extends CommandBase {
  @SuppressWarnings({"PMD.UnusedPrivateField", "PMD.SingularField"})
  private final FiringSubsystem mainSubsystem;
  private double secondsSinceInit;

  /**
   * Creates a new ExampleCommand.
   *
   * @param subsystem The subsystem used by this command.
   */
  public FireCommand(FiringSubsystem subsystem) {
    this.mainSubsystem = subsystem;


    // Use addRequirements() here to declare subsystem dependencies.
    addRequirements(subsystem);
 
  }

  // Called when the command is initially scheduled.
  @Override
  public void initialize() {
      mainSubsystem.enableSpinnerShooter(0.9);
      this.secondsSinceInit = 0;
      
  }

  // Called every time the scheduler runs while the command is scheduled.
  @Override
  public void execute() {
    this.secondsSinceInit += 0.02; // execute is called once every 20 ms
    if (this.secondsSinceInit > 0.7){
        mainSubsystem.setFlap("Open");
        mainSubsystem.magazineOn(Constants.magazineSpeed);
    }
    SmartDashboard.putNumber("Time Since Init", this.secondsSinceInit);
  }

  // Called once the command ends or is interrupted.
  @Override
  public void end(boolean interrupted) {
      mainSubsystem.setFlap("Closed");
      mainSubsystem.disableSpinnerShooter();
      mainSubsystem.magazineOff();
  }

  // Returns true when the command should end.
  @Override
  public boolean isFinished() {
    return false;
  }
}