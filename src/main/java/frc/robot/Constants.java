/*----------------------------------------------------------------------------*/
/* Copyright (c) 2018-2019 FIRST. All Rights Reserved.                        */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot;

/**
 * The Constants class provides a convenient place for teams to hold robot-wide
 * numerical or boolean constants. This class should not be used for any other
 * purpose. All constants should be declared globally (i.e. public static). Do
 * not put anything functional in this class.
 *
 * <p>
 * It is advised to statically import this class (or one of its inner classes)
 * wherever the constants are needed, to reduce verbosity.
 */
public final class Constants
{
    //Drive Motors
    public static final int leftMotorIdF = 0;
    public static final int leftMotorIdB = 1;
    public static final int rightMotorIdF = 3;
    public static final int rightMotorIdB = 4;

    //Control Panel Related
    public static final int cpMotorId = 2;
    public static final double cpMotorSpeed = 0.16;
    public static final int rotateTimesButtonNum = 1;
    public static final int rotateColorButtonNum = 2;
    public static final int driverSliderNum = 3;

    //Drive Related
    public static final int driverJoystickPort = 0;
    public static final int driverTriggerNum = 1;
    public static final int flipButton = 2;
    public static final double wheelsSpeed = -0.5;

    //Action Related
    public static final int commanderJoystickPort = 1;

    //Sensors
    public static final int buttonPort = 1;
    
    public static final float[] yellow = { (float) 0.318848, (float)0.583496, (float)0.097656 };
    public static final float[] blue = { (float) 0.124512, (float)0.403564, (float)0.471924 };
    public static final float[] grean = { (float) 0.138916, (float)0.592529, (float)0.268555 };
    public static final float[] red = { (float) 0.543213, (float)0.342285, (float) 0.114502 };
    public static final float daviation = (float) 0.2;

    //Collection Motors
    public static final int collectionTopPort = 0;
    public static final int collectionBotPort = 1;
    public static final int magazineMotorPort = 3;

    //Collector Related
    public static final double flapMotorSpeed = 0.1;
    public static final double magazineSpeed = 0.7;
    public static final double collectionTopSpeed = 0.5;
    public static final double collectionBotSpeed = 0.5;
    public static final int startCollectingButton = 6;
    public static final int stopCollectingButton = 5;
    public static final int spitButton = 4;

    // Shooting
    public static final int flapPotentumeterAnalogPort = 0;
    public static final int flapMotorPort = 2;

    public static final int collection1Port = 5;
    public static final int collection2Port = 6;
    public static final int magazinePort = 7;
    public static final int flapPort = 8;
    public static final int spinnerShooterId = 5;
    public static final int collectionStorageButtonNum = 1;
    public static final int shooterButtonNum = 3;
    public static final int magazineButtonNum = 5;

    //flapMotor
    public static final double flapOpen = 0.4;
    public static final double flapClosed = 0.75;
}
