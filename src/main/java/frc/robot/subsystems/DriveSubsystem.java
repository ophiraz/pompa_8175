package frc.robot.subsystems;

import edu.wpi.first.wpilibj.Spark;
import edu.wpi.first.wpilibj.SpeedControllerGroup;
import edu.wpi.first.wpilibj.drive.DifferentialDrive;
import edu.wpi.first.wpilibj2.command.SubsystemBase;
import frc.robot.Constants;
import com.ctre.phoenix.motorcontrol.*;
import com.ctre.phoenix.motorcontrol.can.*;

public class DriveSubsystem extends SubsystemBase {
  
  WPI_VictorSPX leftMotorF = new WPI_VictorSPX(Constants.leftMotorIdF);
  WPI_VictorSPX leftMotorB = new WPI_VictorSPX(Constants.leftMotorIdB);
  WPI_VictorSPX rightMotorF = new WPI_VictorSPX(Constants.rightMotorIdF);
  WPI_VictorSPX rightMotorB = new WPI_VictorSPX(Constants.rightMotorIdB);
  DifferentialDrive drive;

  //WPI_TalonSRX talon = new WPI_TalonSRX(Constants.spinnerShooterId);
  //Spark flap = new Spark(Constants.flapMotorPort);

  public DriveSubsystem() 
  {
    this.rightMotorB.follow(this.rightMotorF);
    this.leftMotorB.follow(this.leftMotorF);

    this.drive = new DifferentialDrive(this.leftMotorF, this.rightMotorF);
  }
  public void teleopDrive(double move, double turn)
  {
    this.drive.arcadeDrive(move, turn);
    //this.flap.set(move);
    //this.talon.set(move+1);
  }
  

  public void initDefaultCommand() {
    

  }
  public void periodic() {
   
  }
}


