package frc.robot.subsystems;

import com.ctre.phoenix.motorcontrol.can.WPI_TalonSRX;

import edu.wpi.first.wpilibj.AnalogPotentiometer;
import edu.wpi.first.wpilibj.Spark;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.SubsystemBase;
import frc.robot.Constants;

public class FiringSubsystem extends SubsystemBase{
    private double flapState; // 0.75 is closed, 0.3 is open
    private double flapWantedState;
    private AnalogPotentiometer potentiometer;
    private Spark flapMotor = new Spark(Constants.flapMotorPort);
    WPI_TalonSRX spinnerShooterMotor = new WPI_TalonSRX(Constants.spinnerShooterId);
    private Spark collectionTop;
    private Spark collectionBot;
    private Spark magazineMotor;

    public FiringSubsystem(){
        this.flapWantedState = 0.75;
        this.potentiometer = new AnalogPotentiometer(Constants.flapPotentumeterAnalogPort);
        this.collectionTop = new Spark(Constants.collectionTopPort);
        this.collectionBot = new Spark(Constants.collectionBotPort);
        this.magazineMotor = new Spark(Constants.magazineMotorPort);
        this.collectionBot.setInverted(true);
        this.magazineMotor.setInverted(true);
        this.collectionTop.setInverted(true);
    }

    @Override
    public void periodic() {
        this.flapState = this.potentiometer.get();
        moveFlap(0.3);
        SmartDashboard.putNumber("Potentiometer", this.potentiometer.get());
        SmartDashboard.putNumber("Wanted", this.flapWantedState);

    }

    public void setFlap(String state){
        if (state.equals("Closed")){
            this.flapWantedState = Constants.flapClosed;
        }
        if (state.equals("Open")){
            this.flapWantedState = Constants.flapOpen;
        }
    }

    private void moveFlap(double speed) {
        if ((this.flapState > this.flapWantedState)&&(this.flapWantedState==Constants.flapOpen)){ // opening
            this.flapMotor.set(speed);
        }
        else if ((this.flapState < this.flapWantedState)&&(this.flapWantedState==Constants.flapClosed)){ // closing
            this.flapMotor.set(-1*speed);
        }
        else {
            this.flapMotor.disable();
        }
    }

    public void enableSpinnerShooter(double speed){
        this.spinnerShooterMotor.set(speed);
    }

    public void disableSpinnerShooter(){
        this.spinnerShooterMotor.disable();
    }

    public void magazineOn(double speed){
        this.magazineMotor.set(speed);
    }

    public void magazineOff(){
        this.magazineMotor.disable();
    }

    public void collectorOn(double speedTop, double speedBot){
        this.collectionTop.set(speedTop);
        this.collectionBot.set(speedBot);
    }

    public void collectorOff(){
        this.collectionTop.disable();
        this.collectionBot.disable();
    }


}