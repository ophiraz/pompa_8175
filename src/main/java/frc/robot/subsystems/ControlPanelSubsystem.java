package frc.robot.subsystems;
import edu.wpi.first.wpilibj2.command.SubsystemBase;
import frc.robot.Constants;

import com.revrobotics.ColorSensorV3;

import edu.wpi.first.wpilibj.I2C;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj.util.Color;
import com.revrobotics.ColorMatchResult;
import com.revrobotics.ColorMatch;
import com.ctre.phoenix.motorcontrol.*;
import com.ctre.phoenix.motorcontrol.can.*;

public class ControlPanelSubsystem extends SubsystemBase {
  //Class Variables:  
  private WPI_VictorSPX cpMotor;
  private I2C.Port i2cPort;  //Color sensors variablse:
  private ColorSensorV3 colorSensor;
  //AnalogInput analogButton = new AnalogInput(Constants.buttonPort);

  public ControlPanelSubsystem()
  {
    this.cpMotor = new WPI_VictorSPX(Constants.cpMotorId);
    this.i2cPort  = I2C.Port.kOnboard;
    this.colorSensor = new ColorSensorV3(i2cPort);
  }

  /*
  Input: None
  Output: int, a number representing a color(0=blue, 1=green, 2=red, 3=yellow, 4 = None)
  This Function returns an integer that reprezents a color 
  detected by the sensor from the Control Panel.
  */
  public int getColor(){
    //Color Sensor Part:
    Color detectedColor = this.colorSensor.getColor();
    //The sensor returns a raw IR value of the infrared light detected.
    final ColorMatch m_colorMatcher = new ColorMatch();
    final Color kBlueTarget = ColorMatch.makeColor(0.143, 0.427, 0.429);
    final Color kGreenTarget = ColorMatch.makeColor(0.197, 0.561, 0.240);
    final Color kRedTarget = ColorMatch.makeColor(0.561, 0.232, 0.114);
    final Color kYellowTarget = ColorMatch.makeColor(0.361, 0.524, 0.113);
    int ret = 0;
    ColorMatchResult match;

    m_colorMatcher.addColorMatch(kBlueTarget);
    m_colorMatcher.addColorMatch(kGreenTarget);
    m_colorMatcher.addColorMatch(kRedTarget);
    m_colorMatcher.addColorMatch(kYellowTarget);    
    match = m_colorMatcher.matchClosestColor(detectedColor);

    if (match.color == kBlueTarget) {
      ret = 0;
    } else if (match.color == kRedTarget) {
      ret = 2;
    } else if (match.color == kGreenTarget) {
      ret = 1;
    } else if (match.color == kYellowTarget) {
      ret = 3;
    } else {
      ret = 4;
    }

    // SmartDashboard.putNumber("Red", detectedColor.red);
    // SmartDashboard.putNumber("Green", detectedColor.green);
    // SmartDashboard.putNumber("Blue", detectedColor.blue);
    SmartDashboard.putNumber("Confidence", match.confidence);
    SmartDashboard.putNumber("Detected Color", ret);

    return ret;
  }

  public void cpMotorOn(double speed){
    cpMotor.set(speed);
  }
  public void cpMotorOff()
  {
    cpMotor.disable();
  }
  public void initDefaultCommand() {
    // Set the default command for a subsystem here.
    // setDefaultCommand(new TeleopDriveCommand());
  }
  public void periodic() {
    // This method will be called once per scheduler run
  }

  
}
